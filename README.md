# Pinephone Pro Fedora Mobility Remix

The team is working on getting updated scripts available here using Kickstart & the new Phosh images.

- Current nightlies for Pinephone Pro @ ftp://pine.warpspeed.dk/nightly/pinephone-pro/ - All files on this ftp are highly experimental nightly builds, which are completely untested. You use these at your own risk. All images in this directory are only for PinePhone-PRO.
- They are based on @alho2's custom scripts, firmware not included - install [tow-boot](https://tow-boot.org/getting-started.html) to SPI first

Account in the images are
user: pine
pass: 1111


The Images here expands to up to 12GB - be warned. To avoid expanding it, you can
flash it with Gnome-Disks app, that can read and expand .xz images directly.
If you need larger root partition on image, there is a script in pine homedir
(btrfs-expand-root-part.sh) that will expand the root partition to the full card size.
It is safe to run it, while using your phone, but will use the next up to 10min
to rebalance workload over the partition. You can fully use your phone at the same time,
just do not shut it down, before the script finishes.

10 feb 23

From now Fedora will create an official Phosh image. We now use this image, and
only makes minor changes - to make PinePhone-PRO working, including still using
Megi kernel, as Fedora kernel doesn't support all hw yet.

Also note from this date, we no longer include firmware in the images. You have to
make sure your PinePhone-PRO has working firmware on the SPI chip, before you can
use these images.


## NOTICE

PinePhone-PRO images are work-in-progress - they are not yet fully functional. Known issues now is phone-calls not working.

## Changelog

* 18 mar 22 Plasma-Mobile builds are now available. 
* 30 mar 22 Phosh image added. It is completely untested for now. Reports welcome.
* 07 apr 22 Phosh images are built nightly now. Only tested that they can boot.
* 10 feb 23 Phosh images are now build from Fedoras official Phosh image.
* 10 feb 23 Images do no longer include firmware.